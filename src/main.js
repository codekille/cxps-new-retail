// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
import store from './store'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import MyHeader from './components/common/myHeader'
import MyContent from './components/common/myContent'
import MyFooter from './components/common/myFooter'
Vue.component('my-header', MyHeader);
Vue.component('my-content', MyContent);
Vue.component('my-footer', MyFooter);

Vue.prototype.$axios = 	axios;
Vue.use(ElementUI);
Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
});
