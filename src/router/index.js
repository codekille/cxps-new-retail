import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/common/login'
import Register from '@/components/common/register'
import Layout from '@/components/layout/layout'
import Home from '@/pages/home'
import Product from '@/pages/product'
import UserCenter from '@/pages/userCenter'
import Cart from '@/pages/cart'

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/center',
      name: 'UserCenter',
      component: UserCenter
    },
    {
      path: '/',
      name: 'Layout',
      component: Layout,
      children:[
        {
          path: '/home',
          name: 'Home',
          component: Home
        },
        {
          path: '/product',
          name: 'Product',
          component: Product,
          props: route => ({query: route.query.q})
        },
        {
          path: '/cart',
          name: 'Cart',
          component: Cart
        },
      ]
    }
  ]
})
