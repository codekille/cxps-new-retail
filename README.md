# cxps-new-retail

> 5G新零售2C前端项目

# 技术栈
> vue2.0 + vue-cli + vueRoute +vueX + axios + ElementUI

# Build Setup
``` bash
# 安装路由
npm install vue-router

# 安装 vuex
npm install vuex --save

# 安装 axios
npm install --save axios vue-axios

# 安装 elment ui
npm i element-ui -S

# 运行项目 default port 8088
npm run dev 
```
# 项目预览
## 首页
![Header](/static/img/header.png)
![RX/MS/XP](/static/img/home2.png)
![Recommend](/static/img/home3.png)

## 商品列表
![Product](/static/img/ProductList.png)

## 秒杀

## 活动专区

## 公共底部
![Footer](/static/img/footer.png)
